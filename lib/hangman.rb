class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @referee = players[:referee]
    @guesser = players[:guesser]
  end

  def setup
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = Array.new(word_length, "_")
  end

  def update_board(found_indicies, guess) #args from take_turn
    if found_indicies.length > 0
      found_indicies.each { |idx| @board[idx] = guess}
    end
  end

  def take_turn
    guess = @guesser.guess
    found_indicies = @referee.check_guess(guess)
    update_board(found_indicies, guess)
    @guesser.handle_response(found_indicies, guess)


  end

end #Hangman

class HumanPlayer

end #HumanPlayer

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def secret_word
    @secret_word
  end

  def pick_secret_word
    @secret_word = @dictionary.shuffle[0]
    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def check_guess(letter)
    found_indicies = []
    @secret_word.split("").each_with_index do | l, idx|
      found_indicies << idx if letter == l
    end
    found_indicies
  end

  def guess(board)
    @guess = ("a".."z").to_a.sample
    letter_count = Hash.new(0)
    candidate_words.each do |word|
      word.split("").each do |ch|
        if !board.include?(ch)
          letter_count[ch] += 1
        end
      end
    end
    letter_count.sort_by {|k, v| v }.last[0]

  end

  def handle_response(guess, found_indicies)
    p "found_indicies: #{found_indicies}"
    matching_words = []
    candidate_words.each do |word|
      word_guess_indicies = []
      word.split("").each_with_index do |ch, idx|
        word_guess_indicies << idx if ch == guess
      end #word.split.each
      matching_words << word if word_guess_indicies == found_indicies
    end #candidate_words.select
    @dictionary = matching_words #modifies dictionary!!!
  end

  def candidate_words
    @dictionary.select do |word|
      word.length == @secret_length
    end
  end

end #ComputerPlayer
